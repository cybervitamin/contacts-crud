# Intro 

Task took almost 8h. I had never used laravel before, and I’m not a backend guy so most of time I spent on reading documentation. Some parts of the task were skipped just to save my time: Those are:

1. Email notification
2. Unit Tests


Also I have doubts about some parts that were completed. For CRUD operations I created `web` routes not `api`. By default, breeze vue package come with tailwind css, that I never used before as well. And I am not sure if the chosen approach was optimal or not.

Added .env file for quickstart :)

## Users
- jhon@doe.com / secret - readonly  
- john@smith.com / secret - can create  
- jane@smith.com / secret - can create and delete  
 
## Run
```bash
composer install
php artisan migrate
php artisan db:seed
php artisan serve
```
