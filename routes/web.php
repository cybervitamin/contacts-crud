<?php

use App\Http\Controllers\ContactController;
use Illuminate\Foundation\Application;
use Illuminate\Support\Facades\Route;
use Inertia\Inertia;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return Inertia::render('Welcome', [
//        'canLogin' => Route::has('login'),
//        'canRegister' => Route::has('register'),
//        'laravelVersion' => Application::VERSION,
//        'phpVersion' => PHP_VERSION,
//    ]);
//});

Route::get('/', function () {
    return Inertia::render('App');
})->middleware(['auth', 'verified'])->name('app');

Route::get('/contacts', [ContactController::class, 'index'])
    ->middleware(['auth'])
    ->name('contacts.index');
Route::post('/contacts', [ContactController::class, 'create'])
    ->middleware(['permission:create'])
    ->name('contacts.create');
Route::delete('/contacts/{id}', [ContactController::class, 'destroy'])
    ->middleware(['permission:delete'])
    ->name('contacts.destroy');

require __DIR__.'/auth.php';
