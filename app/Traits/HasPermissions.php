<?php

namespace App\Traits;

use App\Models\Permission;

trait HasPermissions
{

    /**
     * @return mixed
     */
    public function permissions()
    {
        return $this->belongsToMany(Permission::class,'users_permissions');
    }

    /**
     * @param $permission
     * @return bool
     */
    public function hasPermission($permission): bool
    {
        return (bool) $this->permissions()->where('slug', $permission)->count();
    }

    /**
     * @return array
     */

    public function getPermissionsAttribute()
    {
        return $this->permissions()->pluck('slug')->toArray();
    }
}
