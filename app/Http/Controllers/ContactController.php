<?php

namespace App\Http\Controllers;

use App\Models\Contact;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Validator;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index(): Response
    {
        $contacts = Contact::all();
        return response($contacts, 200);
    }

    /**
     * Create a new resource.
     * @param  Request  $request
     * @return Response
     */
    public function create(Request $request): Response
    {
        $validator = Validator::make($request->all(), [
            'firstname' => 'required|string|max:255',
            'lastname' => 'required|string|max:255',
            'email' => 'required|string|email|max:255',
        ]);

        if ($validator->passes()) {
            $contact = Contact::create([
                'firstname' => $request->firstname,
                'lastname' => $request->lastname,
                'email' => $request->email,
            ]);
            return response($contact, 200);
        }

        return response(['error'=>$validator->errors()], 400);
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  Contact  $contact
     * @return Response
     */
    public function destroy(Contact $contact, $id): Response
    {
        $contact = $contact::find($id);
        $contact->delete();
        return response()->noContent();
    }
}
