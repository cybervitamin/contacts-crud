<?php

namespace Database\Seeders;

use App\Models\Permission;
use Illuminate\Database\Seeder;

class PermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $createContact = new Permission();
        $createContact->name = 'Create contacts';
        $createContact->slug = 'create';
        $createContact->save();

        $deleteContact = new Permission();
        $deleteContact->name = 'Delete contacts';
        $deleteContact->slug = 'delete';
        $deleteContact->save();
    }
}
