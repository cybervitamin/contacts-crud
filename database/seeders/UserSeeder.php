<?php

namespace Database\Seeders;

use App\Models\Role;
use App\Models\User;
use App\Models\Permission;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $createContact = Permission::where('slug','create')->first();
        $deleteContact = Permission::where('slug','delete')->first();

        $user1 = new User();
        $user1->name = 'Jhon Doe';
        $user1->email = 'jhon@doe.com';
        $user1->password = bcrypt('secret');
        $user1->save();

        $user2 = new User();
        $user2->name = 'Jhon Smith';
        $user2->email = 'john@smith.com';
        $user2->password = bcrypt('secret');
        $user2->save();
        $user2->permissions()->attach($createContact);

        $user3 = new User();
        $user3->name = 'Jane Smith';
        $user3->email = 'jane@smith.com';
        $user3->password = bcrypt('secret');
        $user3->save();
        $user3->permissions()->attach($createContact);
        $user3->permissions()->attach($deleteContact);
    }
}
