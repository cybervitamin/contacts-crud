<?php

namespace Database\Seeders;

use App\Models\Contact;
use Illuminate\Database\Seeder;

class ContactSeeder extends Seeder
{
    protected $contacts = [
        [ 'firstname' => 'Jhon', 'lastname' => 'Doe', 'email' => 'jhon@doe.com'],
        [ 'firstname' => 'Jane', 'lastname' => 'Doe', 'email' => 'jane@doe.com'],
        [ 'firstname' => 'Jhon', 'lastname' => 'Smith', 'email' => 'jhon@smith.com'],
        [ 'firstname' => 'Jane', 'lastname' => 'Smith', 'email' => 'jane@smith.com'],
    ];
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        foreach ($this->contacts as $contact) {
            $user = new Contact();
            $user->firstname = $contact['firstname'];
            $user->lastname = $contact['lastname'];
            $user->email = $contact['email'];
            $user->save();
        }
    }
}
